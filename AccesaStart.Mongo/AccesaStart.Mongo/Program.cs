﻿using System;
using System.Collections.Generic;
using AccesaStart.Mongo.Models;
using MongoDB.Bson.IO;
using MongoDB.Driver;

namespace AccesaStart.Mongo
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            MongoClient client = new MongoClient();
            IMongoDatabase mongoDatabase = client.GetDatabase("ScoreboardDB");

            var profiles = mongoDatabase.GetCollection<PlayerProfile>("PlayerProfile");




            PlayerProfile andreea = new PlayerProfile
            {
                Name = "Andreea Voda",
                Function = "CEO",
                Score = new Score
                {
                    PlayerTag = 420,
                    PlayerScore = 55,
                    Game = new Game
                    {
                        GameName = "darts"
                    }
                },
                Gender = "Female",
                PlayerColor = "Pink",
            };
            profiles.InsertOne(andreea);
            //var andreeaUpdate = Builders<PlayerProfile>.Update.Set(o => o.Gender, new string "AndreeaVoda");

            //UpdateOne(o => o.Name == "Andreea", costicaUpdate);



            Console.ReadLine();
        }
    }
}
