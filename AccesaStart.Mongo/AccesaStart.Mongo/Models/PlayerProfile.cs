﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace AccesaStart.Mongo.Models
{
    public class PlayerProfile
    {
        [BsonId]
        public ObjectId PlayerId { get; set; }

        public string Name { get; set; }

        public string Function { get; set; }

        public Score Score { get; set; }

        public string Gender { get; set; }

        public string PlayerColor { get; set; }
    }
}
