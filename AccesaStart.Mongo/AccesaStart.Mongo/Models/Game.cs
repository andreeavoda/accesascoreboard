﻿using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace AccesaStart.Mongo.Models
{
    public class Game
    {
        [BsonId]
        public ObjectId GameId { get; set; }

        public string GameName { get; set; }


    }
}
