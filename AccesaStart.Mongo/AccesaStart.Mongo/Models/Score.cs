﻿namespace AccesaStart.Mongo.Models
{
    public class Score
    {
        public int PlayerTag { get; set; }

        public int PlayerScore { get; set; }

        public Game Game { get; set; }
    }
}
